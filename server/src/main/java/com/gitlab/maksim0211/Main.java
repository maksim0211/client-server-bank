package com.gitlab.maksim0211;

import com.gitlab.maksim0211.ATM.System.Transaction.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    static ExecutorService executeIt = Executors.newFixedThreadPool(2);
    /**
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {

        try (ServerSocket server= new ServerSocket(3345)){
            while(true){
                // становимся в ожидание подключения к сокету под именем - "client" на серверной стороне
                Socket client = server.accept();
                executeIt.execute(new MonoThreadClientHandler(client));
                // после хэндшейкинга сервер ассоциирует подключающегося клиента с этим сокетом-соединением
                System.out.print("Connection accepted.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}