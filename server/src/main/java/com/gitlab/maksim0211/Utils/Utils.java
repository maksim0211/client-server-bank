package com.gitlab.maksim0211.Utils;

import com.gitlab.maksim0211.Entity.Account;
import com.gitlab.maksim0211.Entity.DebitCard;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Utils {
    static DebitCard card1 = null;
    static DebitCard card2 = null;
    static DebitCard card3 = null;
    static Account account123456 = null;
    static Account account123457 = null;

    static {
        try {
            card1 = getValidCardForReader();
            card2 = getInvalidCardForReaderByEndDate();
            card3 = getInvalidCardForReaderByCondition();
            account123456 = new Account("123456", 10000.0);
            account123457 = new Account("123457", 150.0);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static void setConditionByCardId(String id){
        //Установка по id карты condition = 0
    }

    public static DebitCard getValidCardForReader() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM");
        return new DebitCard(12312311, "4567", dateFormat.parse("2020/10"),dateFormat.parse("2024/10"),1,100000.0,12340.33);
    }

    public static DebitCard getInvalidCardForReaderByEndDate() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM");
        return new DebitCard(12312312, "4567", dateFormat.parse("2017/09"),dateFormat.parse("2021/09"),1,50000.0,198.99);
    }

    public static DebitCard getInvalidCardForReaderByCondition() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM");
        return new DebitCard(12312313, "4567", dateFormat.parse("2020/11"),dateFormat.parse("2024/11"),0,1000.0,35000.00);
    }

    public static DebitCard findData(Integer id) throws ParseException {
        switch (id) {
            case 12312311:
                return getValidCardForReader();
            case 12312312:
                return getInvalidCardForReaderByEndDate();
            default:
                return getInvalidCardForReaderByCondition();
        }
    }
    public static Account findAccount(String num) {
        switch (num) {
            case "123456":
                return account123456;
            default:
                return account123457;
        }
    }
}
