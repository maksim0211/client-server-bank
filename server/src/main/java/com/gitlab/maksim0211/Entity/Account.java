package com.gitlab.maksim0211.Entity;
// счёт
public class Account {
    public Account(String number, Double balance) {
        this.number = number;
        this.balance = balance;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    private String number;
    private Double balance;

}

