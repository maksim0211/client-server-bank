package com.gitlab.maksim0211;

import com.gitlab.maksim0211.ATM.System.Transaction.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Date;
import java.util.Locale;

public class MonoThreadClientHandler implements Runnable {

    private static Socket client;

    public MonoThreadClientHandler(Socket client) {
        MonoThreadClientHandler.client = client;
    }

    @Override
    public void run() {
        TransactionHandler transactionHandler = new TransactionHandler();
        try {
            // канал записи в сокет
            DataOutputStream out = new DataOutputStream(client.getOutputStream());
            System.out.println("DataOutputStream  created");
            // канал чтения из сокета
            DataInputStream in = new DataInputStream(client.getInputStream());
            System.out.println("DataInputStream created");

            ObjectInputStream ois = new ObjectInputStream(client.getInputStream());

            // начинаем диалог с подключенным клиентом в цикле, пока сокет не закрыт
            while(!client.isClosed()){

                // сервер ждёт в канале чтения (inputstream) получения данных клиента
                String typeTransaction = in.readUTF().toLowerCase(Locale.ROOT);
                Object tcp = ois.readObject();
                String response = null;
                //Можно в дальнейшем вынести в отдельный класс хендлера транзакций
                switch (typeTransaction) {
                    case "transactioncheckpin":
                        response = transactionHandler.checkPin((TransactionCheckPin) tcp);
                        break;
                    case "transactionwithdraw":
                        response = transactionHandler.checkBalance((TransactionWithdrawal) tcp);
                        break;
                    case "transactionconfiscation":
                        response = transactionHandler.setConfiscationToCard((ATMTransaction) tcp);
                        break;
                    case "transactionreceipt":
                        response = transactionHandler.infoAboutAccount((TransactionReceipt) tcp);
                        break;
                    case "transactiontransfer":
                        response = transactionHandler.transfer((TransactionTransfer) tcp);
                        break;

                }

                System.out.println(new Date() + response);

                // если условие окончания работы не верно - продолжаем работу - отправляем эхо-ответ  обратно клиенту
                out.writeUTF(response);

                // освобождаем буфер сетевых сообщений (по умолчанию сообщение не сразу отправляется в сеть, а сначала накапливается в специальном буфере сообщений, размер которого определяется конкретными настройками в системе, а метод  - flush() отправляет сообщение не дожидаясь наполнения буфера согласно настройкам системы
                out.flush();
                break;
            }

            // если условие выхода - верно выключаем соединения
            System.out.println("Client disconnected");
            System.out.println("Closing connections & channels.");

            // закрываем сначала каналы сокета !
            in.close();
            out.close();
            ois.close();

            // потом закрываем сам сокет общения на стороне сервера!
            Thread.sleep(1000);
            client.close();

            // потом закрываем сокет общения с клиентом в нити моносервера
            client.close();

            System.out.println("Closing connections & channels - DONE.");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
