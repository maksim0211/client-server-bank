package com.gitlab.maksim0211.ATM.System.Transaction;
// транзакция справки (?)
public class TransactionReceipt implements ITransaction {
    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getSumLastDeposit() {
        return sumLastDeposit;
    }

    public void setSumLastDeposit(Double sumLastDeposit) {
        this.sumLastDeposit = sumLastDeposit;
    }

    public TransactionReceipt(String accountNum, Double balance, Double sumLastDeposit) {
        this.accountNum = accountNum;
        this.balance = balance;
        this.sumLastDeposit = sumLastDeposit;
    }

    private String accountNum;
    private Double balance;
    private Double sumLastDeposit;
}
