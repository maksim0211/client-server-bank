package com.gitlab.maksim0211.ATM.System.Transaction;

import java.util.Date;

public class TransactionCheckPin implements ITransaction {
    private static final long serialVersionUID = 6529685098267757690L;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    private Date startDate;
    private Date endDate;

    public String getPin() {
        return pin;
    }

    public Integer getCondition() {
        return condition;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TransactionCheckPin(Date startDate, Date endDate, Integer id, String pin, Integer condition) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.id = id;
        this.pin = pin;
        this.condition = condition;
    }

    private Integer id;
    private String pin;
    private Integer condition;
}
