package com.gitlab.maksim0211.ATM.System.Transaction;

import com.gitlab.maksim0211.Entity.DebitCard;
import com.gitlab.maksim0211.Utils.Utils;

import java.text.ParseException;
import java.util.Date;

import static com.gitlab.maksim0211.Utils.Utils.*;

public class TransactionHandler {

    public String checkPin(TransactionCheckPin transactionCheckPin){

        Integer id = transactionCheckPin.getId();
        DebitCard dc = null;
        try {
            dc = findData(id);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(!dc.getPin().equalsIgnoreCase(transactionCheckPin.getPin()))
            return "error:badpin";
        else if(dc.getEndDate().before(new Date()))
            return "error:enddate";
        else if(dc.getCondition() != 1)
            return "error:badcondition";
        else
            return "ok!123456,123457";
    }

    public String checkBalance(TransactionWithdrawal t){
        if(Utils.findAccount(t.getAccountNum()).getBalance() < t.getSum())
            return "error:notenoughmoney";
        else {
            Utils.findAccount(t.getAccountNum()).setBalance(Utils.findAccount(t.getAccountNum()).getBalance() - t.getSum());
            return "ok";
        }
    }

    public String setConfiscationToCard(ATMTransaction t){
        String id = t.getIdCard();
        setConditionByCardId(id);
        return "Конфискация";
    }

    public String infoAboutAccount (TransactionReceipt t){
        Double b = findAccount(t.getAccountNum()).getBalance();
        return "Номер:\t\t\t"+t.getAccountNum()+"\nБаланс:\t\t\t"+b;
    }
    public String transfer (TransactionTransfer t){
        Double b = Utils.findAccount(t.getInitialAccountNum()).getBalance();
        if (t.getSum() > b)
            return "Ошибка: недостаточно денег на счету для перевода.";
        else {
            Double initB = Utils.findAccount(t.getInitialAccountNum()).getBalance();
            Double targetB = Utils.findAccount(t.getTargetAccountNum()).getBalance();
            Utils.findAccount(t.getInitialAccountNum()).setBalance(initB - t.getSum());
            Utils.findAccount(t.getTargetAccountNum()).setBalance(targetB + t.getSum());
            return "ok";
        }
    }

}
