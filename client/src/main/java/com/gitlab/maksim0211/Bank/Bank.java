package com.gitlab.maksim0211.Bank;

public class Bank {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static String name = "ПАО ЗБЕРБАНК";
    public static String address = "Россия, г. Москва, ул. Пушкина дом 16";
}
