package com.gitlab.maksim0211.ATM.System.Account;

//чековый счёт
public class СheckingAccount {
    public СheckingAccount(Double sumLastDeposit) {
        this.sumLastDeposit = sumLastDeposit;
    }

    private Double sumLastDeposit;

    public Double getSumLastDeposit() {
        return sumLastDeposit;
    }

    public void setSumLastDeposit(Double sumLastDeposit) {
        this.sumLastDeposit = sumLastDeposit;
    }
}
