package com.gitlab.maksim0211.ATM.System.Transaction;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class TransactionHandler {
    public String sendTransaction(String clientCommand, ITransaction transaction) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (Socket socket = new Socket("localhost", 3345);
             DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
             DataInputStream dis = new DataInputStream(socket.getInputStream());
             ObjectOutputStream oos = new ObjectOutputStream (socket.getOutputStream());)
        {
            // данные появились - работаем
            // пишем данные с консоли в канал сокета для сервера
            dos.writeUTF(clientCommand);
            dos.flush();

            oos.writeObject(transaction);
            oos.flush();

            // ждём чтобы сервер успел прочесть сообщение из сокета и ответить
            Thread.sleep(4000);

            // если условие разъединения не достигнуто продолжаем работу
            // проверяем, что нам ответит сервер на сообщение(за предоставленное ему время в паузе он должен был успеть ответить)
            while (dis.available() > 0)
                sb.append(dis.readUTF());
        } catch (InterruptedException interruptedException) {

        }
        return sb.toString();
    }
}
