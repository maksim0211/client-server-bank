package com.gitlab.maksim0211.ATM.System.Account;

public class SavingsAccount {
    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public SavingsAccount(Double percent) {
        this.percent = percent;
    }

    private Double percent;
}
