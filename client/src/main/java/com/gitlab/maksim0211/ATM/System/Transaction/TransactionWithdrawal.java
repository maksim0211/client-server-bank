package com.gitlab.maksim0211.ATM.System.Transaction;
//транзакция снятия
public class TransactionWithdrawal implements ITransaction {
    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public TransactionWithdrawal(String accountNum, Double sum, Double balance) {
        this.accountNum = accountNum;
        this.sum = sum;
        this.balance = balance;
    }

    private String accountNum;
    private Double sum;
    private Double balance;
}
