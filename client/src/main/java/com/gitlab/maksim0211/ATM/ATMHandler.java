package com.gitlab.maksim0211.ATM;

import com.gitlab.maksim0211.ATM.System.Subject.DebitCard;
import com.gitlab.maksim0211.ATM.System.Transaction.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

import static com.gitlab.maksim0211.TestData.Utlis.*;

public class ATMHandler {
    public static TransactionHandler transactionHandler = new TransactionHandler();
    public static Scanner scanner = new Scanner(System.in);
    String[] accNums;

    public ATMHandler(ATM atm) {
        this.atm = atm;
    }

    private ATM atm;

    public ATM getAtm() {
        return atm;
    }

    public void setAtm(ATM atm) {
        this.atm = atm;
    }

    public void getStartedMenu() {
        System.out.println("Банкомат запускается...");
        try {
            // TODO: 26.10.2021 Организовать соединение к серверному приложению, пока заглушка
            Thread.sleep(1500L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Добро пожаловать!");
    }

    public boolean getCardMenu() {
        System.out.println("Вставьте карту: (Нажмите Enter)");
        //Нажать энтер
        String t = new Scanner(System.in).nextLine();
        System.out.println("Введите:\n1 - чтобы вставить валидную карту\n2 - карту с истёкшим сроком\n3 - заблокированную карту");
        int cardNum = scanner.nextInt();
        try {
            System.out.println("Чтение...");
            Thread.sleep(1500L);
            //Имитация добавления карты
            switch (cardNum) {
                case 1:
                    atm.getCardReader().readCard(getValidCardForReader());
                    break;
                case 2:
                    atm.getCardReader().readCard(getInvalidCardForReaderByEndDate());
                    break;
                case 3:
                    atm.getCardReader().readCard(getInvalidCardForReaderByCondition());
                    break;
                default:
                    return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    //0 - отмена, 1 - всё ок, 2 - не опознаётся, 3 - ошибка, кофискация
    public int getCheckPinMenu() {
        int cntBadPin = 0;
        while (true) {
            if (cntBadPin == 3) {
                System.out.println("3 раза неверно введён пароль. Конфискация(");
                try {
                    transactionHandler.sendTransaction("transactionconfiscation", new ATMTransaction(
                            null,
                            atm.getCardReader().getCard().getId().toString()
                            , null, null, null));
                } catch (
                        IOException e) {
                    e.printStackTrace();
                }
                return 3;
            }
            System.out.println("Введите пароль или введите -1 для выхода, попыток для ввода пароля: " + (3 - cntBadPin));
            Integer inputedPin = scanner.nextInt();
            String answer = "";
            if (inputedPin != -1) {
                //пример отправки
                try {
                    answer = transactionHandler.sendTransaction("transactionCheckPin", new TransactionCheckPin(
                            atm.getCardReader().getCard().getStartDate(),
                            atm.getCardReader().getCard().getEndDate(),
                            Integer.valueOf(atm.getCardReader().getCard().getId()),
                            inputedPin.toString(),
                            atm.getCardReader().getCard().getCondition()));
                } catch (
                        IOException e) {
                    e.printStackTrace();
                }
                //Отправляем запрос на сервер, пока заглушка. Доступные номера счетов
                if (answer.startsWith("ok")) {
                    accNums = answer.substring(3).split(",");
                    System.out.println("Доступные счета: " + answer.substring(3));
                    return 1;
                } else if (answer.endsWith("badpin")) {
                    cntBadPin++;
                } else if (answer.endsWith("enddate")) {
                    System.out.println("Истёк срок использования карты. Конфискация...");
                    return 2;
                    }
                else if(answer.endsWith("badcondition")) {
                    System.out.println("Карта была ранее заблокирована. Кофискация...");
                    return 3;
                }
            } else {
                System.out.println("Возврат карты...");
                return 0;
            }
        }
    }

    public boolean getActionMenu() {
        System.out.println("Введите:\n1 - чтобы снять деньги\n2 - получить справку\n3 - перевести деньги\n4 - отмена");
        int num = scanner.nextInt();
        int accNum = 0;
        if (num == 4) {
            System.out.println("Возврат карты...");
            return false;
        }
        if (num > 0 && num < 4) {
            while (true) {
                block:
                {
                    scanner.nextLine();
                    if(num != 3)
                        System.out.println("Введите корректный номер счёта:");
                    else
                        System.out.println("Введите номер счёта, с которого необходимо переслать деньги:");
                    final String value = scanner.nextLine();
                    if (Arrays.stream(accNums).filter(s -> s.equals(value)).count() != 1)
                        break block;
                    else {
                        accNum = Integer.parseInt(value);
                        break;
                    }
                }
            }
        }
        try {
            switch (num) {
                case 1:
                    getWithdrawMoneyMenu(accNum);
                    break;
                case 2:
                    getPrintReceiptMenu(accNum);
                    break;
                case 3:
                    getTransferMoneyMenu(accNum);
                    break;
                default:
                    Thread.sleep(1000);
                    System.out.println("Возврат карты...");
                    return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public void getWithdrawMoneyMenu(Integer accountNum) throws InterruptedException {
        System.out.println("Введите сумму, которую хотите снять:");
        final Double value = scanner.nextDouble();
        String answer = "";
        try {
            answer = transactionHandler.sendTransaction("transactionwithdraw", new TransactionWithdrawal(
                    accountNum.toString(),
                    value,
                    null));
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        //Отправляем запрос на сервер, пока заглушка. Доступные номера счетов
        if (answer.startsWith("ok")) {
            System.out.println("Выдача наличных (звук наличных)");
            Thread.sleep(2000);
            System.out.println("Печать чека...");
            Thread.sleep(1000);
            System.out.println("Спасибо за то, что используете наш банк!\n\n\n");
        } else {
            System.out.println("Ошибка! Не хватает средств...");
            System.out.println("Заберите карту...");
        }
    }

    public void getPrintReceiptMenu(Integer accountNum) {
        System.out.println("Начало печати справки...");
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String answer = null;
        System.out.println("----------------------------------------СПРАВКА----------------------------------------");
        System.out.println("\t\t\t\t\t\t\t\t\t\t" + atm.getBank().getName());
        System.out.println("Адрес центрального аппарата банка:\t" + atm.getBank().getAddress());
        System.out.println("Адрес банкомата:\t" + atm.getAddress());
        System.out.println("Номер банкомата:\t" + atm.getId());
        System.out.println("---------------------------------------------------------------------------------------");
        try {
            answer = transactionHandler.sendTransaction("transactionreceipt", new TransactionReceipt(
                    accountNum.toString(),
                    null,
                    null));
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        System.out.println(answer);
        System.out.println("Дата:\t\t\t" + LocalDate.now());
        System.out.println("Время:\t\t\t" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME));
        System.out.println("---------------------------------------------------------------------------------------");
        getActionMenu();
    }

    public void getTransferMoneyMenu(Integer accountNum) {
        Integer accNumTarget = null;
        while (true) {
            block:
            {
                System.out.println("На какой счёт вы хотите положить средства?");
                final String value = scanner.nextLine();
                if (Integer.parseInt(value) == accountNum) {
                    break block;
                } else if (Arrays.stream(accNums).filter(s -> s.equals(value)).count() != 1)
                    break block;
                else {
                    accNumTarget = Integer.parseInt(value);
                    break;
                }
            }
        }
        String answer = null;
        System.out.println("Укажите сумму:");
        Double sum = scanner.nextDouble();
        try {
            System.out.println("Операция выполняется, ожидайте...");
            answer = transactionHandler.sendTransaction("transactionTransfer", new TransactionTransfer(
                    accountNum.toString(),
                    accNumTarget.toString(),
                    sum));
            Thread.sleep(1500);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        if (answer.startsWith("ok"))
            System.out.println("Перевод выполнен успешно");
        else
            System.out.println("Ошибка перевода: " + answer + "\nВыход в меню действий");
        getActionMenu();
    }
}
