package com.gitlab.maksim0211.ATM.System.Subject;

import java.util.Date;

public class DebitCard {
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getCondition() {
        return condition;
    }

    public void setCondition(Integer condition) {
        this.condition = condition;
    }

    public Double getLimit() {
        return limit;
    }

    public void setLimit(Double limit) {
        this.limit = limit;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public DebitCard(Integer id, String pin, Date startDate, Date endDate, Integer condition, Double limit, Double total) {
        this.id = id;
        this.pin = pin;
        this.startDate = startDate;
        this.endDate = endDate;
        this.condition = condition;
        this.limit = limit;
        this.total = total;
    }

    private Integer id;
    private String pin;
    private Date startDate;
    private Date endDate;
    private Integer condition;
    private Double limit;
    private Double total;
}
