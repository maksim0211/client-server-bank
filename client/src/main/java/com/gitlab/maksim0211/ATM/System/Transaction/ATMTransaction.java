package com.gitlab.maksim0211.ATM.System.Transaction;

import java.time.LocalDateTime;

public class ATMTransaction implements ITransaction{
    public String getIdT() {
        return idT;
    }

    public void setIdT(String idT) {
        this.idT = idT;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public LocalDateTime getCurrentDateTime() {
        return currentDateTime;
    }

    public void setCurrentDateTime(LocalDateTime currentDateTime) {
        this.currentDateTime = currentDateTime;
    }

    public Integer getCondition() {
        return condition;
    }

    public void setCondition(Integer condition) {
        this.condition = condition;
    }

    public ATMTransaction(String idT, String idCard, String pin, LocalDateTime currentDateTime, Integer condition) {
        this.idT = idT;
        this.idCard = idCard;
        this.pin = pin;
        this.currentDateTime = currentDateTime;
        this.condition = condition;
    }

    private String idT;
    private String idCard;
    private String pin;
    private LocalDateTime currentDateTime;
    private Integer condition;
}
