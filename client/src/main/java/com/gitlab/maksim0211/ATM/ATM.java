package com.gitlab.maksim0211.ATM;

import com.gitlab.maksim0211.ATM.System.Device.CashDispenser;
import com.gitlab.maksim0211.ATM.System.Device.ReceiptPrinter;
import com.gitlab.maksim0211.ATM.System.Device.CardReader;
import com.gitlab.maksim0211.Bank.Bank;

public class ATM {

    private Bank bank = new Bank();

    public Bank getBank() {
        return bank;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String id = "7766677";
    private String address = "г. Москва, улица Алексея Долматова, дом 2007 строение 2";

    public CardReader getCardReader() {
        return cardReader;
    }

    public ReceiptPrinter getReceiptPrinter() {
        return receiptPrinter;
    }

    public CashDispenser getCashDispenser() {
        return cashDispenser;
    }

    public ATM(CardReader cardReader, ReceiptPrinter receiptPrinter, CashDispenser cashDispenser) {
        this.cardReader = cardReader;
        this.receiptPrinter = receiptPrinter;
        this.cashDispenser = cashDispenser;
    }

    private CardReader cardReader;
    private ReceiptPrinter receiptPrinter;
    private CashDispenser cashDispenser;

}
