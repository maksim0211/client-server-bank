package com.gitlab.maksim0211.ATM.System.Device;

import com.gitlab.maksim0211.ATM.System.Subject.DebitCard;

public class CardReader {
    public DebitCard getCard() {
        return card;
    }

    private DebitCard card;

    public void readCard(DebitCard debitCard){
        this.card = debitCard;
    }
}
