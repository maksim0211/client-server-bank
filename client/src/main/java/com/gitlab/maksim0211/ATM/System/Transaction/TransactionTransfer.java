package com.gitlab.maksim0211.ATM.System.Transaction;

public class TransactionTransfer implements ITransaction {
    public String getInitialAccountNum() {
        return initialAccountNum;
    }

    public void setInitialAccountNum(String initialAccountNum) {
        this.initialAccountNum = initialAccountNum;
    }

    public String getTargetAccountNum() {
        return targetAccountNum;
    }

    public void setTargetAccountNum(String targetAccountNum) {
        this.targetAccountNum = targetAccountNum;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public TransactionTransfer(String initialAccountNum, String targetAccountNum, Double sum) {
        this.initialAccountNum = initialAccountNum;
        this.targetAccountNum = targetAccountNum;
        this.sum = sum;
    }

    private String initialAccountNum;
    private String targetAccountNum;
    private Double sum;
}
