package com.gitlab.maksim0211.TestData;

import com.gitlab.maksim0211.ATM.System.Subject.DebitCard;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Utlis {
    public static DebitCard getValidCardForReader() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM");
        return new DebitCard(12312311, null,null,null,null,null,null);
    }

    public static DebitCard getInvalidCardForReaderByEndDate() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM");
        return new DebitCard(12312312, null,null,null,null,null,null);
    }

    public static DebitCard getInvalidCardForReaderByCondition() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM");
        return new DebitCard(12312313, null,null,null,null,null,null);
    }
}
