package com.gitlab.maksim0211;

import com.gitlab.maksim0211.ATM.ATM;
import com.gitlab.maksim0211.ATM.ATMHandler;
import com.gitlab.maksim0211.ATM.System.Device.CardReader;
import com.gitlab.maksim0211.ATM.System.Device.CashDispenser;
import com.gitlab.maksim0211.ATM.System.Device.ReceiptPrinter;

public class Main {

    public static void main(String[] args) {
        ATM ATMObject = new ATM(new CardReader(),new ReceiptPrinter(),new CashDispenser());
        ATMHandler atmHandler = new ATMHandler(ATMObject);
        atmHandler.getStartedMenu();
        while (true){
            block:
            {
                if (!atmHandler.getCardMenu())
                    break block;
                int tmp = atmHandler.getCheckPinMenu();
                switch (tmp){
                    case 1:
                        atmHandler.getActionMenu();
                        break;
                    default:
                        break block;
                }
            }
        }
    }
}